#pragma comment(lib,"sfml-graphics-d.lib")
#pragma comment(lib,"sfml-audio-d.lib")
#pragma comment(lib,"sfml-system-d.lib")
#pragma comment(lib,"sfml-window-d.lib")
#pragma comment(lib,"sfml-network-d.lib")

#include <assert.h>
#include <memory>

#include "SFML\Graphics.hpp"

sf::RenderWindow window;


struct Person
{
	std::unique_ptr<sf::Clock> m_timer;
	sf::RectangleShape m_sprite;
	sf::RectangleShape m_outline;
	sf::Texture m_texture;
	sf::Shader m_shader;
}m_steve;

bool moveRight = true;
const sf::Vector2f move = sf::Vector2f(0.0f, 1.0f);

void processEvents()
{
	sf::Event evnt;
	while (window.pollEvent(evnt))
	{
		if (evnt.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

void init()
{
	assert(m_steve.m_texture.loadFromFile("texture.png"));
	m_steve.m_sprite.setPosition(400.0f, 300.0f);
	m_steve.m_sprite.setTexture(&m_steve.m_texture, true);
	m_steve.m_sprite.setSize(sf::Vector2f(400.0f, 400.0f));
	m_steve.m_outline = m_steve.m_sprite;

	m_steve.m_outline.setFillColor(sf::Color::Transparent);
	m_steve.m_outline.setOutlineThickness(2.0f);
	m_steve.m_outline.setOutlineColor(sf::Color::White);
	m_steve.m_outline.setSize(m_steve.m_sprite.getSize() + sf::Vector2f(10.0f, 10.0f));

	{
		const auto & size = m_steve.m_sprite.getSize();
		m_steve.m_sprite.setOrigin(size * 0.5f);
	}
	{
		const auto & size = m_steve.m_outline.getSize();
		m_steve.m_outline.setOrigin(size * 0.5f);
	}

	assert(m_steve.m_shader.loadFromFile("vertex_shader.vert", "frag_shader.frag"));

	m_steve.m_timer = std::make_unique<sf::Clock>();
}

void update(const float & dt)
{
	const auto & halfSize = m_steve.m_sprite.getSize() * 0.5f;
	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W))
	{
		m_steve.m_sprite.move(0.0f, -1.0f);
	}

	m_steve.m_outline.setPosition(m_steve.m_sprite.getPosition());
}

void render(const float & dt)
{
	m_steve.m_shader.setUniform("time", m_steve.m_timer->getElapsedTime().asSeconds());
	//m_steve.m_shader.setUniform("size", m_steve.m_sprite.getSize());
	m_steve.m_shader.setUniform("size", sf::Glsl::Vec2(800.0, 600.0f));
	m_steve.m_shader.setUniform("offset", m_steve.m_sprite.getPosition());
	window.clear();
	window.draw(m_steve.m_outline);
	window.draw(m_steve.m_sprite, &m_steve.m_shader);
	window.display();
}

/// <summary>
/// Main entry point for the application
/// </summary>
/// <param name="argc">number of arguments passed into the application</param>
/// <param name="argv">c style string of the arguments passed into the application</param>
int main(int argc, char** argv)
{
	init();

	window.create(sf::VideoMode(800u, 600u), "SFML Testing Shader", sf::Style::Default);

	sf::Clock clock;
	const float & updateDT = 1.0f / 60.0f;
	float lag = updateDT;
	float renderDT = 0.0f;
	while (window.isOpen())
	{
		renderDT = clock.restart().asSeconds();
		lag += renderDT;
		processEvents();
		while (lag > updateDT)
		{
			update(updateDT);
			lag -= updateDT;
		}
		render(renderDT);

	}
}